// When the DOM tree is loaded, add eventlistener to the add button
// and generate the tabs
document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('addButton').addEventListener('click', addTab);
  initializeView();
});

// Store the tabs that's been saved's info
var bookmarks = [];

// Grabs tab being saved's info and stores it
function addTab() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    // No need to use forEach because we are only expecting 1 tab
    var tab = tabs[0];
    bookmarks.push(new Bookmark(tab.title, tab.url, tab.favIconUrl));

    // "Commits" the stored tabs into Chrome
    chrome.storage.sync.set({'link': bookmarks});
    // Re-populate the UI for each saved tab in the extension
    updateView();
    // Close the current tab that was just saved.
    // NOTE: In the future, might be good to have config that toggles behavior
    chrome.tabs.remove(tab.id);
  });
}

// Finds the specific saved tab marked for deletion and removes it
// from the list
function removeTab(bookmark) {
  var index = bookmarks.map(function(e) {return e.url;}).indexOf(bookmark.url);
  if(index > -1) {
    bookmarks.splice(index, 1);
  }

  // Update the list of stored tabs in Chrome with the new list
  chrome.storage.sync.set({'link': bookmarks});
  // Refresh the UI
  updateView();
}

// Iterates through all the saved tabs and creates a "book" for each
function updateView() {
  // Locate the container designated to hold the saved tabs from DOM tree
  var bookmarkShelf = document.getElementById('tabContainer');
  bookmarkShelf.innerHTML = "";
  bookmarks.forEach(function(bookmark) {
    // Use prepend to add to the top of list (newest to oldest ordering)
    bookmarkShelf.prepend(createBook(bookmark));
  });
}

// Creates all the HTML elements and attributes for each tab saved
function createBook(bookmark) {
  var bookBinding = document.createElement("article");
  bookBinding.classList.add("bookBinding");
  var favIcon = document.createElement("img");
  var favIconSrcAtt = document.createAttribute("src");
  var favIconAltAtt = document.createAttribute("alt");
  favIconAltAtt.value = "Favicon";
  favIcon.classList.add("favIcon");
  var textContentWrapper = document.createElement("div");
  textContentWrapper.classList.add("textContentWrapper");
  var spanTitle = document.createElement("h2");
  spanTitle.classList.add("title");
  var spanURL = document.createElement("a");
  spanURL.classList.add("url");
  var deleteButton = document.createElement("button");
  deleteButton.classList.add("deleteButton");
  deleteButton.addEventListener('click', function(e){
    e.stopPropagation();
    removeTab(bookmark);
  });
  bookBinding.addEventListener('click', function(){
    chrome.tabs.create({'url': bookmark.url}, removeTab(bookmark));
  });
  if(typeof bookmark.favIcon !== 'undefined') {
    favIconSrcAtt.value = bookmark.favIcon;
  }
  else {
    favIconSrcAtt.value = "src/BookmarkIcon.svg";
  }
  favIcon.setAttributeNode(favIconSrcAtt);
  favIcon.setAttributeNode(favIconAltAtt);
  spanTitle.innerHTML = bookmark.title;
  spanURL.innerHTML = bookmark.url;
  textContentWrapper.append(spanTitle, spanURL);
  bookBinding.append(favIcon, textContentWrapper, deleteButton);
  return bookBinding;
}

// Called when opening the extension popup for the first time
function initializeView() {
  // Grabs all the tabs saved in Chrome
  chrome.storage.sync.get('link', function(link){
    if (link["link"] != undefined) {
      bookmarks = link["link"];
    } else {
      bookmarks = [];
    }
    // Using that list, generate the UI for each saved tab
    updateView();
    chrome.storage.sync.set({'link': bookmarks});
  });
}

// Bits and pieces of the Bookmark "object"
function Bookmark(title, url, favicon) {
  this.title = title;
  this.url = url;
  this.favIcon = favicon;
}
